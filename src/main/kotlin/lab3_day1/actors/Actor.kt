package lab3_day1.actors

import lab3_day1.Percept
import lab3_day1.action.Action

interface Actor  {
    val name : String
    fun act() : Action
    fun perceive(vararg facts : Percept) : Unit
}