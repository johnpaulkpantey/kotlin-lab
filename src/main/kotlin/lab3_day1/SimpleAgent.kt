package lab3_day1

import lab3_day1.action.Action
import lab3_day1.action.ForageAction
import lab3_day1.actors.Actor

class SimpleAgent(override val name: String) : Actor {
    override fun act(): Action {
        return ForageAction()
    }

    override fun perceive(vararg facts: Percept) {
        TODO("Not yet implemented")
    }

    override fun toString(): String {
        return "Your name is $name"
    }
}