package lab3_day1.environment

import lab3_day1.Percept
import lab3_day1.action.Action
import lab3_day1.action.ForageAction
import lab3_day1.actors.Actor
import kotlin.random.Random

class FoodEnvironment(vararg ags: Actor): Environment(*ags) {
    var scores: MutableMap<Actor, Int> = mutableMapOf()
    var animal: String? = null

    init {
        ags.forEach {
            scores[it] = 0
        }
    }
    override fun processAction(agent: Actor, act: Action) {
        if (act is ForageAction) scores[agent] = scores[agent]!! +1
    }

    override fun sense(agent: Actor) {
//        if (animal is null) agent.perceive() else Percept("animal",[name])

    }


    override fun step() {
        animal = if (generateDouble() >=0.5) "Goat" else null
        super.step()
    }

    private fun generateDouble(): Double{
        return  Random.nextDouble(0.0, 1.0)
    }
}