var funds : Double = 100.0
val pswd = "password"

fun main() {
    var input : String
    var cmd : List<String>

    while (true) {
        print("Command (balance, deposit , withdraw ): ")
        input = readLine()!!
        cmd = input.split(" ")
        try {
            when (cmd[0]) {
                // Each command goes here...
                "balance" -> balance()
                "deposit" -> deposit(cmd[1].toDouble())
                "withdraw" -> withdraw(cmd[1].toDouble())
                else -> println("Invalid command")
            }

        } catch(e: Exception){
            when(e){
                is IndexOutOfBoundsException -> print("You must provide an option (amount) for ${cmd[0]} action. Try again.\n")
                is NumberFormatException -> print("You must provide a number amount for ${cmd[0]} action. Try again.\n")
            }

        }
    }
}
fun balance(): Unit{
    println("Your balance: $funds")
}

fun deposit(amount : Double): Double{
    funds += amount
    println("You have deposited GHc $amount")
    println("Your total amount is GHc $funds")
    return funds
}
fun withdraw(amount: Double): Unit{
    print("Provide password to make a withdrawal: ")
    val password = readLine()!!
    if(password.isEmpty() || password != pswd) {
        println("Provide correct password. Try again")
    }
    else {
        if (amount> funds){
            println("Sorry you don't have enough funds")
        }else{
            funds -= amount
            println("You withdrew GHc $amount")
            println("Your total balance is GHc $funds")
        }

    }

}