fun main(): Unit {
    val hello: String ="hello world"
    println("$hello, This is Paul")
    println("======================================================================")
    for(i in -5..5){
    println("Position Y = ${calculate(3, i,-1)}")
    }
    println("======================================================================")
    for(i in 0..20){
        val y1 = calculate(3, i,5)
        val y2 = calculate(4, i,2)
        if (y1 == y2){
            println("Position Y1 and Y2 intersect at Y= $y1 and X = $i")
        }
    }
    println("1. ======================================================================\n")
    intersection(3,5,4,2)
    println("2. ======================================================================\n")
    intersection(2,1,3,3)
    println("3. ======================================================================\n")
    intersection(-2,3,3,-2)

}

fun calculate(m: Int, x: Int, c: Int): Int{
    return m * x + c
}
fun intersection(mOne : Int, cOne : Int, mTwo : Int, cTwo : Int) : Unit {
    // This function takes the equations of two lines:
    // yOne = mOne * x + cOne and yTwo = mTwo * x + cTwo
    // and prints the X value they intersect at
    // or prints "Don't intersect" if they do not.
    // You only need to check x values between 0 and 100
    for(i in 0..100){
    val y1 = mOne * i + cOne
    val y2 = mTwo * i + cTwo
        if (y1 != y2){
            println("Don't intersect")
        }
        else{
            println(i)
        }
    }
}